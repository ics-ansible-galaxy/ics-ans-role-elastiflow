# ics-ans-role-elastiflow

Ansible role to install ElastiFlow -> https://github.com/robcowart/elastiflow

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
---
elastiflow_elasticsearch_image: docker.elastic.co/elasticsearch/elasticsearch-oss
elastiflow_elasticsearch_tag: 6.6.1
elastiflow_elasticsearch_data: /var/lib/elastiflow_es
elastiflow_elasticsearch_port: 9200
elastiflow_elasticsearch_Xms: "{{ elastiflow_elasticsearch_Xmx }}"
elastiflow_elasticsearch_Xmx: 3g
elastiflow_elasticsearch_cluster_name: elastiflow

elastiflow_kibana_image: docker.elastic.co/kibana/kibana-oss
elastiflow_kibana_tag: 6.6.1
elastiflow_kibana_port: 5601

elastiflow_logstash_image: robcowart/elastiflow-logstash-oss
elastiflow_logstash_tag: 3.4.1_6.1.3
elastiflow_logstash_Xms: "{{ elastiflow_logstash_Xmx }}"
elastiflow_logstash_Xmx: 3g

elastiflow_dns_host: 8.8.8.8

elastiflow_network: elastiflow-net
elastiflow_hostname: "{{ ansible_fqdn }}"
elastiflow_dashboard_file: elastiflow.kibana.6.6.x.json

elastiflow_curator_image: registry.esss.lu.se/ics-docker/elasticsearch_curator
elastiflow_curator_tag: 5.7.6
elastiflow_curator_config_path: /opt/curator
elastiflow_curator_retention_days: 30
```
Please set the java HEAP according to https://github.com/robcowart/elastiflow/blob/master/INSTALL.md

## Example Playbook

```yaml
- hosts: elastiflows
  roles:
    - role: ics-ans-role-elastiflow
```

## TODO
- Implement AA(A)
- Import the saved objects automatically

## License

BSD 2-clause
