import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('elastiflows')


def test_csentry_index(host):
    cmd1 = host.command('sudo docker exec elastiflow-kibana curl -L http://127.0.0.1:5601')
    assert '/bundles/app/kibana/bootstrap.js' in cmd1.stdout
